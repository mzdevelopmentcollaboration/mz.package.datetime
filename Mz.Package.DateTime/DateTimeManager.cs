﻿using System;

namespace Mz.Package.DateTime
{
    public class DateTimeManager
    {
        public enum CommonTimeZones
        {
            CEST
        }

        public static System.DateTime GetCurrentDateTime(CommonTimeZones timeZone)
        {
            switch (timeZone)
            {
                case CommonTimeZones.CEST:
                {
                    return FindCurrentDateTime("Central Europe Standard Time");
                }
                default:
                {
                    return System.DateTime.UtcNow;
                }
            }
        }

        public static System.DateTime GetCurrentDateTime(string timeZoneId)
        {
            return FindCurrentDateTime(timeZoneId);
        }

        private static System.DateTime FindCurrentDateTime(string timeZoneId)
        {
            try
            {
                return TimeZoneInfo.ConvertTimeFromUtc(System.DateTime.UtcNow,
                    TimeZoneInfo.FindSystemTimeZoneById(timeZoneId));
            }
            catch
            {
                return System.DateTime.MinValue;
            }
        }
    }
}
